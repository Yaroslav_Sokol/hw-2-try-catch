// 1. 1. Коли ми хочемо внести якесь виключення в функції, наприклад ділення на 0 в калькуляторі, завдяки try ... catch 
//       ми можемо контрулювать обробку помилок, типу при діленні на 0 вибиває помилку.
//    2. Також коли ми працюємо з бекендом/базами даних, також доречно використовувать try ... catch для запитів 
//       (Теж саме з мережевими запитами через посилання).
//    3. Також читав що використовується для роботи з файлами, але як саме точно сказать не можу.

const list = document.getElementById('root'); // Звертаємось до <div id="root">
const ul = document.createElement("ul"); // Створюємо <ul> елемент

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

// Додаємо <ul> до <div id="root">
list.appendChild(ul);

books.forEach(book => {
    try {
        if (!book.author || !book.name || !book.price) {
            const missInfo = [];
            if (!book.author) {missInfo.push('author')};
            if (!book.name) {missInfo.push('name')};
            if (!book.price) {missInfo.push('price')};

            throw new Error(`Книгі не вистачає властивості - ${missInfo}`);
        }

        const li = document.createElement("li");
        const bookInfo = `Назва: ${book.name}, Автор: ${book.author}, Ціна: ${book.price}`;
        li.textContent = bookInfo;
        ul.appendChild(li);
    } catch (error) {
        console.error(error.message);
    }
});